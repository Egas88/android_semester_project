package ru.itmo.musicplayer

import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import org.junit.Assert
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class UITest {

    @get:Rule
    var activityRule: ActivityTestRule<PlayerActivity> = ActivityTestRule(PlayerActivity::class.java)

    @Test
    fun checkAudioData() {
        val songDurationText = onView(withId(R.id.TVseekBarEnd))

        // Проверяем, что элемент существует на экране
        songDurationText.check(matches(isDisplayed()))
        songDurationText.check(matches(withText(formatDuration(MainActivity.musicService!!.mediaPlayer!!.duration.toLong()))))
    }
}