package ru.itmo.musicplayer

import android.app.PendingIntent
import android.app.Service
import android.content.Intent
import android.graphics.BitmapFactory
import android.media.MediaPlayer
import android.os.Binder
import android.os.Handler
import android.os.IBinder
import android.os.Looper
import android.support.v4.media.session.MediaSessionCompat
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions

class MusicService : Service() {

    private var myBinder = MyBinder()
    var mediaPlayer: MediaPlayer? = null
    private lateinit var mediaSession: MediaSessionCompat
    private lateinit var runnable: Runnable

    override fun onBind(intent: Intent?): IBinder? {
        mediaSession = MediaSessionCompat(baseContext, "My Music")
        return myBinder
    }

    inner class MyBinder: Binder() {
        fun currentService():MusicService {
            return this@MusicService
        }
    }

    fun showNotification(playPauseBtn:Int) {

        val prevIntent = Intent(baseContext, NotificationReciever::class.java).setAction(ApplicationClass.PREVIOUS)
        val prevPendingIntent = PendingIntent.getBroadcast(baseContext, 0, prevIntent, PendingIntent.FLAG_UPDATE_CURRENT)

        val nextIntent = Intent(baseContext, NotificationReciever::class.java).setAction(ApplicationClass.NEXT)
        val nextPendingIntent = PendingIntent.getBroadcast(baseContext, 0, nextIntent, PendingIntent.FLAG_UPDATE_CURRENT)

        val playIntent = Intent(baseContext, NotificationReciever::class.java).setAction(ApplicationClass.PLAY)
        val playPendingIntent = PendingIntent.getBroadcast(baseContext, 0, playIntent, PendingIntent.FLAG_UPDATE_CURRENT)

        val exitIntent = Intent(baseContext, NotificationReciever::class.java).setAction(ApplicationClass.EXIT)
        val exitPendingIntent = PendingIntent.getBroadcast(baseContext, 0, exitIntent, PendingIntent.FLAG_UPDATE_CURRENT)

        val imageArt = NowPlaying.musicListNP?.get(NowPlaying.curPos)?.let { getImgArt(it.path) }

        val image = if (imageArt != null) {
            BitmapFactory.decodeByteArray(imageArt, 0, imageArt.size)
        } else {
            BitmapFactory.decodeResource(resources, R.drawable.music_splash)
        }

        val notification = NotificationCompat.Builder(baseContext, ApplicationClass.CHANNEL_ID)
            .setContentTitle(NowPlaying.musicListNP?.get(NowPlaying.curPos)?.title)
            .setContentText(NowPlaying.musicListNP?.get(NowPlaying.curPos)?.artist)
            .setSmallIcon(R.drawable.note_icon)
            .setLargeIcon(image)
            .setStyle(androidx.media.app.NotificationCompat.MediaStyle().setMediaSession(mediaSession.sessionToken))
            .setPriority(NotificationCompat.PRIORITY_HIGH)
            .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
            .setOnlyAlertOnce(true)
            .addAction(R.drawable.previous_icon, "Previous", prevPendingIntent)
            .addAction(playPauseBtn, "Play", playPendingIntent)
            .addAction(R.drawable.next_icon, "Next", nextPendingIntent)
            .addAction(R.drawable.exit_icon, "Exit", exitPendingIntent)
            .build()

        startForeground(13, notification)
    }

    fun createMediaPlayer(musicList: ArrayList<Music>, pos:Int){
        try{
            if (MainActivity.musicService?.mediaPlayer == null) MainActivity.musicService?.mediaPlayer = MediaPlayer()
            MainActivity.musicService?.mediaPlayer!!.reset()
            MainActivity.musicService?.mediaPlayer!!.setDataSource(musicList[pos].path)
            MainActivity.musicService?.mediaPlayer!!.prepare()
            MainActivity.musicService?.mediaPlayer!!.start()
            MainActivity.musicService!!.showNotification(R.drawable.pause_icon)
            MainActivity.musicService!!.mediaPlayer!!.setOnCompletionListener(MediaPlayer.OnCompletionListener {
                setSongPosition(increment = true)
                NowPlaying.musicListNP?.let { it1 ->
                    MainActivity.musicService!!.createMediaPlayer(
                        it1, NowPlaying.curPos)
                }
                try {
                    //For Player
                    Glide.with(this)
                        .load(NowPlaying.musicListNP?.get(NowPlaying.curPos)?.artUri)
                        .apply(RequestOptions().placeholder(R.drawable.music_splash).centerCrop())
                        .into(PlayerActivity.binding.songImg)
                    MainActivity.musicService!!.seekBarSetup()
                    PlayerActivity.binding.songName.text = NowPlaying.musicListNP?.get(NowPlaying.curPos)?.title
                    PlayerActivity.binding.TVseekBarStart.text = formatDuration(MainActivity.musicService!!.mediaPlayer!!.currentPosition.toLong())
                    PlayerActivity.binding.TVseekBarEnd.text = formatDuration(MainActivity.musicService!!.mediaPlayer!!.duration.toLong())
                    PlayerActivity.binding.seekBarPA.progress = 0
                    PlayerActivity.binding.seekBarPA.max = MainActivity.musicService!!.mediaPlayer!!.duration
                    if (PlayerActivity.repeat) PlayerActivity.binding.repeatBtnPA.setColorFilter(ContextCompat.getColor(this, R.color.purple_500))

                    //For NP
                    NowPlaying.binding?.let { it1 ->
                        Glide.with(this)
                            .load(NowPlaying.musicListNP?.get(NowPlaying.curPos)?.artUri)
                            .apply(RequestOptions().placeholder(R.drawable.music_splash).centerCrop())
                            .into(it1.songImgNP)
                    }
                    NowPlaying.binding?.songNameNP?.text = NowPlaying.musicListNP?.get(NowPlaying.curPos)?.title
                }catch (e:Exception){ }
            })
        } catch (e: java.lang.Exception) {
            return
        }
    }

    fun seekBarSetup() {
        runnable = Runnable {
            PlayerActivity.binding.TVseekBarStart.text = formatDuration(mediaPlayer!!.currentPosition.toLong())
            PlayerActivity.binding.seekBarPA.progress = mediaPlayer!!.currentPosition
            Handler(Looper.getMainLooper()).postDelayed(runnable, 200)
        }
        Handler(Looper.getMainLooper()).postDelayed(runnable, 0)
    }
}