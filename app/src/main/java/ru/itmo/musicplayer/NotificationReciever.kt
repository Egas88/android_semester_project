package ru.itmo.musicplayer

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions

class NotificationReciever: BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        when(intent?.action) {
            ApplicationClass.PREVIOUS -> prevNextSong(increment = false, context = context!!)
            ApplicationClass.NEXT -> prevNextSong(increment = true, context = context!!)
            ApplicationClass.PLAY -> if (MainActivity.isPlaying) pauseMusic() else playMusic()
            ApplicationClass.EXIT -> {
                MainActivity.musicService!!.stopForeground(true)
                MainActivity.isPlaying = false
                MainActivity.musicService!!.mediaPlayer!!.pause()
                if (PlayerActivity.isSet) PlayerActivity.binding.playPauseButton.setIconResource(R.drawable.play_icon)
                NowPlaying.binding?.playPauseBtnNP?.setIconResource(R.drawable.play_icon)
            }
        }
    }

    private fun playMusic() {
        MainActivity.isPlaying = true
        MainActivity.musicService!!.mediaPlayer!!.start()
        MainActivity.musicService!!.showNotification(R.drawable.pause_icon)
        if (PlayerActivity.isSet) PlayerActivity.binding.playPauseButton.setIconResource(R.drawable.pause_icon)
        NowPlaying.binding?.playPauseBtnNP?.setIconResource(R.drawable.pause_icon)
    }

    private fun pauseMusic() {
        MainActivity.isPlaying = false
        MainActivity.musicService!!.mediaPlayer!!.pause()
        MainActivity.musicService!!.showNotification(R.drawable.play_icon)
        if (PlayerActivity.isSet) PlayerActivity.binding.playPauseButton.setIconResource(R.drawable.play_icon)
        NowPlaying.binding?.playPauseBtnNP?.setIconResource(R.drawable.play_icon)
    }

    private fun prevNextSong(increment:Boolean, context: Context) {
        setSongPosition(increment = increment)
        NowPlaying.musicListNP?.let { MainActivity.musicService!!.createMediaPlayer(it, NowPlaying.curPos) }
        NowPlaying.binding?.let {
            Glide.with(context)
                .load(NowPlaying.musicListNP?.get(NowPlaying.curPos)?.artUri)
                .apply(RequestOptions().placeholder(R.drawable.music_splash).centerCrop())
                .into(it.songImgNP)
        }
        NowPlaying.binding?.songNameNP?.text = NowPlaying.musicListNP?.get(NowPlaying.curPos)?.title
        if (PlayerActivity.isSet) {
            Glide.with(context)
                .load(NowPlaying.musicListNP?.get(NowPlaying.curPos)?.artUri)
                .apply(RequestOptions().placeholder(R.drawable.music_splash).centerCrop())
                .into(PlayerActivity.binding.songImg)
            PlayerActivity.binding.songName.text = NowPlaying.musicListNP?.get(NowPlaying.curPos)?.title
            playMusic()
        }
    }
}