package ru.itmo.musicplayer

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.media.MediaPlayer
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import ru.itmo.musicplayer.databinding.MusicViewBinding


class MusicAdapter(private val context: Context, private var musicList: ArrayList<Music>) : RecyclerView.Adapter<MusicAdapter.MyHolder>() {

    class MyHolder(binding: MusicViewBinding) : RecyclerView.ViewHolder(binding.root) {
        val title = binding.SongNameMV
        val album = binding.songAlbumMV
        val image = binding.imageMV
        val duration = binding.songDuration
        val root = binding.root
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyHolder {
        return MyHolder(MusicViewBinding.inflate(LayoutInflater.from(context), parent, false))
    }

    override fun onBindViewHolder(holder: MyHolder, position: Int) {
        holder.title.text = musicList[position].title
        holder.album.text = musicList[position].album
        holder.duration.text = formatDuration(musicList[position].duration)
        Glide.with(context)
            .load(musicList[position].artUri)
            .apply(RequestOptions().placeholder(R.drawable.music_splash).centerCrop())
            .into(holder.image)

        holder.root.setOnClickListener{
            if (NowPlaying.curPos != position) MainActivity.isPlaying = false
            if (PlayerActivity.shuffle) {
                PlayerActivity.shuffle = false
                musicList = ArrayList()
                MainActivity.MusicListMA?.let { it1 -> musicList.addAll(it1) }
            }
            fillFragment(position)
            NowPlaying.curPos = position
            NowPlaying.musicListNP = musicList
            MainActivity.musicService?.createMediaPlayer(musicList = musicList, position)
            if (MainActivity.isPlaying) pauseMusic()
            else playMusic()
        }
    }

    override fun getItemCount(): Int {
        return musicList.size
    }

    @SuppressLint("NotifyDataSetChanged")
    fun updateMusicList(searchList:ArrayList<Music>) {
        musicList = ArrayList()
        musicList.addAll(searchList)
        notifyDataSetChanged()
    }

    private fun playMusic() {
        MainActivity.musicService!!.mediaPlayer!!.start()
        MainActivity.isPlaying = true
        NowPlaying.binding?.playPauseBtnNP?.setIconResource(R.drawable.pause_icon)
    }

    private fun pauseMusic() {
        MainActivity.musicService!!.mediaPlayer!!.pause()
        MainActivity.isPlaying = false
        NowPlaying.binding?.playPauseBtnNP?.setIconResource(R.drawable.play_icon)
    }

    private fun fillFragment(pos: Int) {
        NowPlaying.binding?.let {
            Glide.with(context)
                .load(musicList[pos].artUri)
                .apply(RequestOptions().placeholder(R.drawable.music_splash).centerCrop())
                .into(it.songImgNP)
        }
        NowPlaying.binding?.songNameNP?.text = musicList[pos].title
        NowPlaying.binding?.root?.visibility = View.VISIBLE
    }
}