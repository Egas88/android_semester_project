package ru.itmo.musicplayer

import android.annotation.SuppressLint
import android.content.Intent
import android.media.MediaPlayer
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import ru.itmo.musicplayer.databinding.FragmentNowPlayingBinding

class  NowPlaying : Fragment(){

    companion object {
        @SuppressLint("StaticFieldLeak")
        var binding: FragmentNowPlayingBinding? = null
        var musicListNP : ArrayList<Music>? = null
        var curPos = 0
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_now_playing, container, false)
        binding = FragmentNowPlayingBinding.bind(view)
        binding!!.root.visibility = View.INVISIBLE

        initializePlaylist()

        binding!!.playPauseBtnNP.setOnClickListener{
            if (MainActivity.isPlaying) pauseMusic() else playMusic()
        }
        binding!!.nextBtnNP.setOnClickListener {
            prevNextSong(increment = true)
        }
        binding!!.PreviousButtonNP.setOnClickListener {
            prevNextSong(increment = false)
        }

        binding!!.root.setOnClickListener {
            val intent = Intent(activity, PlayerActivity::class.java)
            startActivity(intent)
        }

        return view
    }

    private fun initializePlaylist() {
        if (MainActivity.search) {
            musicListNP = ArrayList()
            MainActivity.MusicListSearch?.let { musicListNP!!.addAll(it) }
            if (PlayerActivity.shuffle)  musicListNP!!.shuffle()
            setFragment()
        } else {
            musicListNP = ArrayList()
            MainActivity.MusicListMA?.let { musicListNP!!.addAll(it) }
            if (PlayerActivity.shuffle)  musicListNP!!.shuffle()
            setFragment()
        }
    }

    private fun setFragment() {
        if (musicListNP?.size != 0) {
            binding?.let {
                Glide.with(this)
                    .load(musicListNP?.get(curPos)?.artUri)
                    .apply(RequestOptions().placeholder(R.drawable.music_splash).centerCrop())
                    .into(it.songImgNP)
            }
            binding?.songNameNP?.text = musicListNP?.get(curPos)?.title
        }
    }

    override fun onResume() {
        super.onResume()
        if (MainActivity.musicService != null) {
            binding?.root?.visibility = View.VISIBLE
            setFragment()
            if (MainActivity.isPlaying) binding?.playPauseBtnNP?.setIconResource(R.drawable.pause_icon)
            else binding?.playPauseBtnNP?.setIconResource(R.drawable.play_icon)
        }
    }

    private fun playMusic() {
        MainActivity.musicService!!.mediaPlayer!!.start()
        binding?.playPauseBtnNP?.setIconResource(R.drawable.pause_icon)
        MainActivity.isPlaying = true
    }

    private fun pauseMusic() {
        MainActivity.musicService!!.mediaPlayer!!.pause()
        binding?.playPauseBtnNP?.setIconResource(R.drawable.play_icon)
        MainActivity.isPlaying = false
    }

    private fun prevNextSong(increment:Boolean) {
        setSongPosition(increment = increment)
        musicListNP?.let { MainActivity.musicService!!.createMediaPlayer(it, curPos) }
        setFragment()
        playMusic()
    }
}