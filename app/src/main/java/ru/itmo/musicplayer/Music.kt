package ru.itmo.musicplayer

import android.media.MediaMetadataRetriever
import java.util.concurrent.TimeUnit

data class Music(
    val id:String,
    val title:String,
    val album:String,
    val artist:String,
    val duration:Long=0,
    val path:String,
    val artUri: String,
)

fun formatDuration(duration:Long):String {
    val minutes = TimeUnit.MINUTES.convert(duration, TimeUnit.MILLISECONDS)
    val seconds = (TimeUnit.SECONDS.convert(duration, TimeUnit.MILLISECONDS) -
            minutes* TimeUnit.SECONDS.convert(1, TimeUnit.MINUTES))
    return String.format("%02d:%02d", minutes, seconds)
}

fun getImgArt(path:String): ByteArray? {
    val mmr = MediaMetadataRetriever()
    mmr.setDataSource(path)
    return mmr.embeddedPicture
}

fun setSongPosition(increment: Boolean) {
    if (!PlayerActivity.repeat) {
        if (increment) {
            if (NowPlaying.musicListNP!!.size - 1 == NowPlaying.curPos)
                NowPlaying.curPos = 0
            else ++NowPlaying.curPos
        } else {
            if (0 == NowPlaying.curPos)
                NowPlaying.curPos = NowPlaying.musicListNP!!.size - 1
            else --NowPlaying.curPos
        }
    }
}