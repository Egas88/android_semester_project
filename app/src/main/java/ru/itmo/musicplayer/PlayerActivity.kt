package ru.itmo.musicplayer

import android.annotation.SuppressLint
import android.content.Intent
import android.media.MediaPlayer
import android.media.audiofx.AudioEffect
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.SeekBar
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import ru.itmo.musicplayer.databinding.ActivityPlayerBinding

class PlayerActivity : AppCompatActivity() {

    companion object {
        @SuppressLint("StaticFieldLeak")
        lateinit var binding: ActivityPlayerBinding
        var repeat: Boolean = false
        var shuffle:Boolean = false
        var isSet = false;
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setTheme(R.style.coolPink)
        binding = ActivityPlayerBinding.inflate(layoutInflater)
        isSet = true
        setContentView(binding.root)

        setLayout()
        if (MainActivity.isPlaying) binding.playPauseButton.setIconResource(R.drawable.pause_icon)
        else binding.playPauseButton.setIconResource(R.drawable.play_icon)


        binding.backButton.setOnClickListener {
            finish()
        }

        binding.playPauseButton.setOnClickListener{
            if (MainActivity.isPlaying) pauseMusic() else playMusic()
        }

        binding.PreviousButton.setOnClickListener {
            prevNextSong(increment = false)
        }
        binding.nextButton.setOnClickListener {
            prevNextSong(increment = true)
        }

        binding.repeatBtnPA.setOnClickListener{
            if (!repeat) {
                repeat = true
                Toast.makeText(this, "Song will repeat", Toast.LENGTH_SHORT).show()
                binding.repeatBtnPA.setColorFilter(ContextCompat.getColor(this, R.color.purple_500))
            } else {
                repeat = false
                binding.repeatBtnPA.setColorFilter(ContextCompat.getColor(this, R.color.cool_pink))
            }
        }

        binding.shuffleButton.setOnClickListener {
            if (shuffle) {
                shuffle = false
                NowPlaying.musicListNP = MainActivity.MusicListMA!!
                binding.shuffleButton.setColorFilter(ContextCompat.getColor(this, R.color.cool_pink))
            } else {
                shuffle = true
                NowPlaying.musicListNP?.shuffle()
                Toast.makeText(this, "Songs shuffled", Toast.LENGTH_SHORT).show()
                binding.shuffleButton.setColorFilter(ContextCompat.getColor(this, R.color.purple_500))
            }
        }

        binding.seekBarPA.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener{
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                if (fromUser) MainActivity.musicService!!.mediaPlayer!!.seekTo(progress)
            }
            override fun onStartTrackingTouch(seekBar: SeekBar?) = Unit
            override fun onStopTrackingTouch(seekBar: SeekBar?) = Unit

        })

        binding.equalizerButton.setOnClickListener{
            try {
                val EqIntent = Intent(AudioEffect.ACTION_DISPLAY_AUDIO_EFFECT_CONTROL_PANEL)
                EqIntent.putExtra(
                    AudioEffect.EXTRA_AUDIO_SESSION,
                    MainActivity.musicService!!.mediaPlayer!!.audioSessionId
                )
                EqIntent.putExtra(AudioEffect.EXTRA_PACKAGE_NAME, baseContext.packageName)
                EqIntent.putExtra(AudioEffect.EXTRA_CONTENT_TYPE, AudioEffect.CONTENT_TYPE_MUSIC)
                startActivityForResult(EqIntent, 13)
            } catch (e:Exception) {Toast.makeText(this, "Equalizer feature not supported", Toast.LENGTH_SHORT).show()}
        }
    }

    private fun setLayout() {
        Glide.with(this)
            .load(NowPlaying.musicListNP?.get(NowPlaying.curPos)?.artUri)
            .apply(RequestOptions().placeholder(R.drawable.music_splash).centerCrop())
            .into(binding.songImg)
        MainActivity.musicService!!.seekBarSetup()
        binding.songName.text = NowPlaying.musicListNP?.get(NowPlaying.curPos)?.title
        binding.TVseekBarStart.text = formatDuration(MainActivity.musicService!!.mediaPlayer!!.currentPosition.toLong())
        binding.TVseekBarEnd.text = formatDuration(MainActivity.musicService!!.mediaPlayer!!.duration.toLong())
        binding.seekBarPA.progress = 0
        binding.seekBarPA.max = MainActivity.musicService!!.mediaPlayer!!.duration
        if (repeat) binding.repeatBtnPA.setColorFilter(ContextCompat.getColor(this, R.color.purple_500))
        if (shuffle) binding.shuffleButton.setColorFilter(ContextCompat.getColor(this, R.color.purple_500))
    }

    private fun playMusic() {
        MainActivity.musicService!!.mediaPlayer!!.start()
        binding.playPauseButton.setIconResource(R.drawable.pause_icon)
        NowPlaying.binding?.playPauseBtnNP?.setIconResource(R.drawable.pause_icon)
        MainActivity.isPlaying = true
    }

    private fun pauseMusic() {
        MainActivity.musicService!!.mediaPlayer!!.pause()
        binding.playPauseButton.setIconResource(R.drawable.play_icon)
        NowPlaying.binding?.playPauseBtnNP?.setIconResource(R.drawable.play_icon)
        MainActivity.isPlaying = false
    }

    private fun prevNextSong(increment:Boolean) {
        setSongPosition(increment = increment)
        NowPlaying.musicListNP?.let { MainActivity.musicService!!.createMediaPlayer(it, NowPlaying.curPos) }
        setLayout()
        playMusic()
    }

    @Deprecated("Deprecated in Java")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 13 || resultCode == RESULT_OK) {
            return
        }
    }
}